// NOAA SAME Transmitter

// Use the DDS at 16666.666... Hz, gives us a 32:1 oversample.
#define DDS_REFCLK_DEFAULT 16666.666
#define SAMPLERATE 16666.666
#define BITRATE    520.83
#define MARK       2083.3
#define SPACE      1562.5
#define SYMBOLTIME 32

#include <Wire.h>
#include "DDS.h"
#include "AFSK.h"

DDS dds;
AFSK afsk;

void setup() {
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  dds.start();
  afsk.start(&dds);
  dds.on();
  dds.setAmplitude(255);
}

void loop() {
  // Send an AFSK frame
  const char frame[] = "ZCZC-WXR-RWT-029095-029091+0615-3241530-KTST/NWS-";
  AFSK::Packet *packet = AFSK::PacketBuffer::makePacket(strlen(frame));
  packet->print(frame);
  Serial.print(millis());
  Serial.println(" putPacket");
  afsk.encoder.putPacket(packet);
  if(afsk.txReady()) {
    Serial.print(millis());
    Serial.println(" txReady is true");
    dds.on();
    Serial.print(millis());
    Serial.println(" DDS on");
    if(afsk.txStart()) {
      Serial.print(millis());
      Serial.println(" txStart success");
      while(!afsk.isDone()) {
        delay(10);
      }
      Serial.print(millis());
      Serial.println(" isDone true");
    } else {
      Serial.println("txStart failed");
    }
    dds.off();
  }
  Serial.println("Sleeping");
  delay(1000);
}

ISR(ADC_vect) {
  static uint8_t tcnt=0;
  TIFR1 = _BV(ICF1);
  dds.clockTick();
  if(++tcnt == (DDS_REFCLK_DEFAULT/BITRATE)) {
    afsk.timer();
    tcnt = 0;
  }
}
